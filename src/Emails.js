import useFetch from "./custom-hooks/useFetch";
import React from 'react'

export default function Emails() {
  const [data] = useFetch("https://jsonplaceholder.typicode.com/users/");

  return (
    <>
      {data &&
        data.map((item) => {
          return <p key={item.id}>{item.email}</p>;
        })}
    </>
  );
};