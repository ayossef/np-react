import useFetch from "./custom-hooks/useFetch";
import React from 'react'

export default function Posts() {
  const [data] = useFetch("https://jsonplaceholder.typicode.com/posts");

  return (
    <>
      {data &&
        data.map((item) => {
          return <p key={item.id}>{item.title}</p>;
        })}
    </>
  );
};