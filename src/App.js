
import React from 'react';

import Posts from './Posts';
import Emails from './Emails';


export default function App() {
  return (
    <div>
     <h1>
      List of Emails
     </h1>
     <Emails></Emails>
     <h1>
      List of Posts
     </h1>
     <Posts></Posts>
    </div>
  );
}